<?php
	/*use Twig_Autoloader;
	use Twig_Loader_Filesystem as Twig_Loader_Filesystem;
	use Twig_Environment as Twig_Environment;*/
	namespace Controlador;
	/**
	* 
	*/
	class Controlador {
		protected $twig;
		protected $loader;

		function __construct() {
			
		}

		private function inicializarTwig() {
			$this->loader = new \Twig_Loader_Filesystem('./vistas/');
			/*
				Lista de carpetas originales del Framework
			*/
			//$this->loader->addPath('./vistas/admin/', 'admin');
			//$this->loader->addPath('./vistas/errores/', 'errores');

			$this->twig = new \Twig_Environment($this->loader, array(
				//'cache' => './app/storage/cacheTwig',
			));
		}
		public function vista($vista = NULL, $datos = []) {
			if(!$vista) {
				echo 'No se establecio una vista'; exit;
			}

			if(!$this->loader) {
				$this->inicializarTwig();
			}

			/*
				Carga los datos globales de Twig antes de renderizar
			*/
			new \Sistema\TwigGlobal($this->twig);

			echo $this->twig->render($vista . '.twig.php', $datos);
			exit;
		}

		public function getLoader() {
			return $this->loader;
		}

		public function makeFolder($nombre = NULL, $ruta = NULL) {
			if(!$this->loader) {
				$this->inicializarTwig();
			}
			if(!$nombre || !$ruta) {
				echo 'Faltan datos al agregar la carpeta de vistas'; exit;
			}
			$this->loader->addPath($ruta, $nombre);
			return;
		}
	}