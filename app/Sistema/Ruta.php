<?php 
	namespace Sistema;
	use Sistema\AltoRouter as AltoRouter;
	/**
	* 
	*/
	class Ruta {

		private $rutas;
		private $AR;
		private $controlador;

		function __construct($rutas) {
			$this->rutas = $rutas;
			if(sistema('sistema')['desarrollo']) $this->encontrarControlador($this->rutas['rutas']);
			$this->AR = new AltoRouter();
			if($this->rutas['controlador']) {
				$this->controlador = $this->rutas['controlador'];
			}
			$this->cargarRutas();
		}

		private function cargarRutas() {
			if($this->rutas['rutas']) {
				$this->AR->addRoutes($this->rutas['rutas']);
			}
			$this->ejecutar();
		}

		private function ejecutar() {
			$match = $this->AR->match();
			if(!$match) {
				header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
			}

			// Verificar requisito
			if($match['requisito']) {
				$r = "\\Requisito\\{$match['requisito']}";
				$p = new $r();
				if(!$p->start()) {
					$p->error();
					exit;
				}
			}
			// - - -

			if(is_callable($match['target'])) {
				call_user_func_array($match['target'], $match['params']);
				exit;
			} else {
				
				$controlador = explode('|', $match['target']);
				$cntrllr = ($this->controlador) ? $this->controlador . '\\' . $controlador[0] : $controlador[0];
				if(method_exists($cntrllr, @$controlador[1])) {
					call_user_func_array(array(new $cntrllr, $controlador[1]), array_values($match['params']));
				}
			}
		}

		private function encontrarControlador($rutas) {
			$controladores = [];
			$requisitos = [];
			foreach ($rutas as $ruta) {
				if(is_string($ruta[2])) {
					$controlador = explode('|', $ruta[2]);
					if(!@$controladores[$controlador[0]]) $controladores[$controlador[0]] = [];
					$controladores[$controlador[0]] []= $controlador[1];
				}
				if(@$ruta[4]) {
					$requisitos []= $ruta[4];
				}
			}
			// Busca y si no encuentra el Controlador, lo crea
			foreach ($controladores as $controlador => $metodos) {
				$C = new \CreadorControladores($controlador, $metodos);
				$C->ejecutar();
			}
			// Busca y si no encuentra el Requisito, lo crea
			foreach ($requisitos as $requisito) {
				$C = new \CreadorRequisitos($requisito);
				$C->ejecutar();
			}
		}
	}