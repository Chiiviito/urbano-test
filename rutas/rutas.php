<?php
	$rutas = [
		'rutas' => [
			['GET', '/', 'SitioControlador|index', 'indexSitio'],
			// Rutas AJAX
			//['GET', '/listar/[*:tabla]', 'SitioControlador|listar']
			['GET', '/listar/clientes', 'SitioControlador|listarClientes'],
			['GET', '/listar/grupo-clientes', 'SitioControlador|listarGrupoClientes'],

			['POST', '/editar/clientes', 'SitioControlador|editarCliente'],
			['POST', '/editar/grupo-clientes', 'SitioControlador|editarGrupoCliente']
		],
		'controlador' => 'Controlador'
	];