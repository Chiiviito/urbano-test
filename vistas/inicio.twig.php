{% extends "plantilla.twig.php" %}

{% block titulo %}Urbano - AMB{% endblock %}

{% block contenido %}
	<script>
		var clientes = JSON.parse('{{ clientes_json|raw }}');
		var grupos = JSON.parse('{{ grupos_clientes_json|raw }}');
		var cliente = {
			id: "",
			nombre: "",
			apellido: "",
			email: "",
			grupo_cliente: "",
			observaciones: ""
		};
		var grupo = {
			id: "",
			nombre: ""
		};
		function limpiarVariables() {
			cliente = {
				id: "",
				nombre: "",
				apellido: "",
				email: "",
				grupo_cliente: "",
				observaciones: ""
			};
			grupo = {
				id: "",
				nombre: ""
			};
			cargarClienteVariablesEnInputs(cliente);
			cargarGrupoVariablesEnInputs(grupo);
		}

		function cargarClienteVariablesEnInputs(cliente) {
			$('#cliente_nombre').val(cliente.nombre);
			$('#cliente_apellido').val(cliente.apellido);
			$('#cliente_email').val(cliente.email);
			$('#cliente_grupo_cliente').val(cliente.grupo_cliente);
			$('#cliente_observaciones').val(cliente.observaciones);
		}
		function cargarGrupoVariablesEnInputs(grupo) {
			$('#grupo_nombre').val(grupo.nombre);
		}
		//-------
		function guardarClienteJson() {
			cliente.nombre = $('#cliente_nombre').val();
			cliente.apellido = $('#cliente_apellido').val();
			cliente.email = $('#cliente_email').val();
			cliente.grupo_cliente = $('#cliente_grupo_cliente').val();
			cliente.observaciones = $('#cliente_observaciones').val();
		}
		function guardarGrupoJson() {
			grupo.nombre = $('#grupo_nombre').val();
		}
		
		$(function() {
			$('.btn-modal').on('click', function() {
				limpiarVariables();
				// Si el btn no tiene un data-id entonces no busca
				if($(this).data('cliente')) {
					for (let i = 0; i < clientes.length; i++) {
						if(clientes[i].id == $(this).data('cliente')) {
							cliente = clientes[i];
							break;
						}
					}
					cargarClienteVariablesEnInputs(cliente);
				}
				// Si el btn no tiene un data-id entonces no busca
				if($(this).data('grupo')) {
					for (let i = 0; i < grupos.length; i++) {
						if(grupos[i].id == $(this).data('grupo')) {
							grupo = grupos[i];
							break;
						}
					}
					cargarGrupoVariablesEnInputs(grupo);
				}
			});
			$('#guardarGrupo').on('click', function() {
				guardarGrupoJson();
				console.log('Datos enviados', grupo);
				$.ajax({
					// la URL para la petición
					url : 'editar/grupo-clientes',
					data : grupo,
					type : 'POST',
					dataType : 'json',
					success : function(json) {
						location.reload(true);
					},
					error : function(xhr, status) {
						console.log(xhr);
					}
				});
			});
			$('#guardarCliente').on('click', function() {
				guardarClienteJson();
				console.log('Datos enviados', cliente);
				$.ajax({
					// la URL para la petición
					url : 'editar/clientes',
					data : cliente,
					type : 'POST',
					dataType : 'json',
					success : function(json) {
						location.reload(true);
					},
					error : function(xhr, status) {
						console.log(xhr);
					}
				});
			});
		});

	</script>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<h1>Urbano ABM</h1>
				<hr>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3 offset-md-3">
				<h3>Clientes</h3>
			</div>
			<div class="col-md-3">
				<button class="btn btn-success float-right btn-modal" data-toggle="modal" data-target="#modalCliente">+</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 offset-md-3">			
				<table class="table table-bordered table-stripped">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Grupo de cliente</th>
							<th width="5%">Editar</th>
						</tr>
					</thead>
					<tbody>
						{% for cliente in clientes %}
						<tr>
							<td>{{ cliente.nombre }}</td>
							<td>{{ cliente.apellido }}</td>
							<td>{{ cliente.email }}</td>
							<td>{{ cliente.grupo_cliente }}</td>
							<td><button class="btn btn-primary btn-modal" data-cliente="{{ cliente.id }}" data-toggle="modal" data-target="#modalCliente">Editar</button></td>
						</tr>
						{% endfor %}
					</tbody>
				</table>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 offset-md-3">
				<h3>Grupos de clientes</h3>
			</div>
			<div class="col-md-3">
				<button class="btn btn-success float-right btn-modal" data-toggle="modal" data-target="#modalGrupo">+</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<table class="table table-bordered table-stripped">
					<thead>
						<tr>
							<th>Nombre</th>
							<th width="5%">Editar</th>
						</tr>
					</thead>
					<tbody>
						{% for grupo in grupos_clientes %}
						<tr>
							<td>{{ grupo.nombre }}</td>
							<td><button class="btn btn-primary btn-modal" data-grupo="{{ grupo.id }}" data-toggle="modal" data-target="#modalGrupo">Editar</button></td>
						</tr>
						{% endfor %}
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Modal cliente -->
	<div class="modal fade" id="modalCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Cliente</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label>Nombre</label>
					<input type="text" class="form-control" id="cliente_nombre">
					<label>Apellido</label>
					<input type="text" class="form-control" id="cliente_apellido">
					<label>Email</label>
					<input type="text" class="form-control" id="cliente_email">
					<label>Grupo cliente</label>
					<select id="cliente_grupo_cliente" class="form-control">
						{% for grupo in grupos_clientes %}
						<option value="{{ grupo.nombre }}">{{ grupo.nombre }}</option>
						{% endfor %}
					</select>

					<label>Observaciones</label>
					<textarea  id="cliente_observaciones" class="form-control" rows="5"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-primary" id="guardarCliente">Guardar</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal grupo-->
	<div class="modal fade" id="modalGrupo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Grupo</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label>Nombre</label>
					<input type="text" class="form-control" id="grupo_nombre">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerar</button>
					<button type="button" class="btn btn-primary" id="guardarGrupo">Guardar</button>
				</div>
			</div>
		</div>
	</div>
{% endblock %}